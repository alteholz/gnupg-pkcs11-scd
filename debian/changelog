gnupg-pkcs11-scd (0.10.0-5) unstable; urgency=medium

  * fix FTBFS against assuan 3
    Thanks a lot to Andreas Metzler for the patch
    (Closes: #1079927)

 -- Thorsten Alteholz <debian@alteholz.de>  Sat, 31 Aug 2024 19:08:42 +0200

gnupg-pkcs11-scd (0.10.0-4) unstable; urgency=medium

  * configure.ac: use pkgconf instead of libgcrypt-config (Closes: #1071864)
  * configure.ac: use pkgconf instead of libassuan-config (Closes: #1072276)

 -- Thorsten Alteholz <debian@alteholz.de>  Thu, 20 Jun 2024 19:08:42 +0200

gnupg-pkcs11-scd (0.10.0-3) unstable; urgency=medium

  * debian/control: don't suggest transition package gnupg2
                    (Closes: #1055399)

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 05 Nov 2023 19:08:42 +0100

gnupg-pkcs11-scd (0.10.0-2) unstable; urgency=medium

  * add patch to detect libgpg-error by pkg-config now
    (Closes: #1022313)

 -- Thorsten Alteholz <debian@alteholz.de>  Sat, 05 Nov 2022 22:08:42 +0200

gnupg-pkcs11-scd (0.10.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control: bump standard to 4.6.1 (no changes)
  * debian/control: use dh11
  * debian/control: add Rules-Requires-Root:

 -- Thorsten Alteholz <debian@alteholz.de>  Thu, 23 Jun 2022 22:08:42 +0200

gnupg-pkcs11-scd (0.9.2-1) sid; urgency=medium

  * New upstream release
  * debian/control: bump standard to 4.3.0 (no changes)
  * debian/patches: all patches applied upstream

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 03 Feb 2019 18:57:24 +0100

gnupg-pkcs11-scd (0.9.1-2) unstable; urgency=medium

  * debian/control: bump standard to 4.2.1 (no changes)
  * debian/control: use Salsa VCS URLs

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 09 Sep 2018 15:05:01 +0200

gnupg-pkcs11-scd (0.9.1-1) unstable; urgency=medium

  * New upstream release
  * debian/watch: development is now at github
  * debian/control: bump standard to 4.1.3 (no changes)
  * debian/control: use dh11
  * debian/control: add proxy package
  * debian/patches: update patches
  * debian/rules: avoid "--with autotools_dev"
  * debian/rules: add hardening flags
  * debian/gnupg-pkcs11-scd.examples: new path for example file

 -- Thorsten Alteholz <debian@alteholz.de>  Thu, 25 Jan 2018 21:28:01 +0100

gnupg-pkcs11-scd (0.7.3-4) unstable; urgency=medium

  * debian/control: Recompile against libssl-dev. (Closes: #851077)
  * debian/control: depend on libgcrypt20-dev (Closes: #864106)
  * debian/control: bump standard to 4.1.1 (no changes)
  * debian/control: use dh10
  * debian/control: remove gnupg-pkcs11-scd-dbg

 -- Thorsten Alteholz <debian@alteholz.de>  Mon, 04 Dec 2017 19:07:31 +0100

gnupg-pkcs11-scd (0.7.3-3) unstable; urgency=medium

  * debian/control: switch to libssl1.0-dev (Closes: #848778)
                    (this has been done by pkcs11-helper and I have
                     to follow)

 -- Thorsten Alteholz <debian@alteholz.de>  Tue, 20 Dec 2016 16:07:31 +0100

gnupg-pkcs11-scd (0.7.3-2) unstable; urgency=medium

  * New maintainer (Closes: #761831)
  * add openssl-1.1.0.patch (Closes: #828328)
  * debian/control: set dh version to >= 9
  * debian/control: bump standard to 3.9.8 (no changes)

 -- Thorsten Alteholz <debian@alteholz.de>  Thu, 17 Nov 2016 18:07:31 +0100

gnupg-pkcs11-scd (0.7.3-1) unstable; urgency=low

  * New maintainers. Closes: #543936
  * Imported Upstream version 0.7.3
  * Added Vcs-* fields
  * Moved *.examples to /usr/share/doc/gnupg-pkcs11-scd/examples
  * Update d/compat to 8
  * Bump debhelper to (>= 8)
  * Added '--with autotools_dev' option in dh to replace
    config.(guess|sub) removal

 -- Fabrizio Regalli <fabreg@fabreg.it>  Tue, 20 Sep 2011 00:07:31 +0200

gnupg-pkcs11-scd (0.7.2-1) unstable; urgency=low

  * QA upload.
  * New upstream version. (Closes: #593528)
    - Use libassuan version 2. (Closes: #614492)
  * Update debhelper dependency to 7.0.50,
    since debian/rules uses 'override_dh_' targets.
  * Change the level of dependency on gnupg|gpgsm to Suggests.
    (Closes: #593529)
  * Add watch file.
  * Update debian/copyright to DEP5.
  * Bump to Standards-Version 3.9.2. No changes required.

 -- Mònica Ramírez Arceda <monica@probeta.net>  Sat, 02 Jul 2011 20:15:37 +0200

gnupg-pkcs11-scd (0.06-7) unstable; urgency=low

  * Removing vcs fields.
  * Orphaning package.

 -- Daniel Baumann <daniel@debian.org>  Thu, 27 Aug 2009 16:32:50 +0200

gnupg-pkcs11-scd (0.06-6) unstable; urgency=low

  * Passing destdir manually to debhelper (Closes: #542227).
  * Wrapping depends.
  * Updating package to standards version 3.8.3.

 -- Daniel Baumann <daniel@debian.org>  Tue, 18 Aug 2009 19:35:58 +0200

gnupg-pkcs11-scd (0.06-5) unstable; urgency=low

  * Replacing obsolete dh_clean -k with dh_prep.
  * Updating section of the debug package.
  * Prefixing debhelper files with package name.
  * Upgrading package to standards version 3.8.2.
  * Updating year in copyright file.
  * Minimizing rules file.
  * Adding misc depends.
  * Using correct rfc-2822 date formats in changelog.

 -- Daniel Baumann <daniel@debian.org>  Sun, 02 Aug 2009 12:24:38 +0200

gnupg-pkcs11-scd (0.06-4) unstable; urgency=low

  * Updating vcs fields in control file.
  * Adding debug package.

 -- Daniel Baumann <daniel@debian.org>  Sat, 20 Sep 2008 22:19:00 +0200

gnupg-pkcs11-scd (0.06-3) unstable; urgency=medium

  * Reordering rules file.
  * Rewriting copyright file in machine-interpretable format.
  * Adding vcs fields in control file.
  * Upgrading package to debhelper 7.
  * Reverting config.guess and config.sub to upstream.
  * Updating to standards 3.8.0.

 -- Daniel Baumann <daniel@debian.org>  Fri, 20 Jun 2008 10:09:00 +0200

gnupg-pkcs11-scd (0.06-2) unstable; urgency=medium

  * Correcting gnupg depends.

 -- Daniel Baumann <daniel@debian.org>  Mon, 31 Mar 2008 11:34:00 +0200

gnupg-pkcs11-scd (0.06-1) unstable; urgency=low

  * New upstream release.

 -- Daniel Baumann <daniel@debian.org>  Fri, 07 Mar 2008 17:05:00 +0100

gnupg-pkcs11-scd (0.05-1) unstable; urgency=low

  * Initial release (Closes: #409595).

 -- Daniel Baumann <daniel@debian.org>  Fri, 07 Dec 2007 14:56:00 +0100
